package demo.java.day2;

import java.sql.*;

public class JDBCExampleJava {
    public static void main(String[] args) {
        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            Class.forName("org.h2.Driver");
            con = DriverManager.getConnection("jdbc:h2:/tmp/test","sa", "");
            stmt = con.createStatement();
            rs = stmt.executeQuery("SELECT * FROM public.kommune");
            while (rs.next()) {
                System.out.println("kommune id: " + rs.getLong(1) +
                        ", navn: " + rs.getString(2) +
                        ", areal: " + rs.getString(3));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                rs.close();
            } catch (Exception e) {
            }
            try {
                stmt.close();
            } catch (Exception e) {
            }
            try {
                con.close();
            } catch (Exception e) {
            }
        }
    }
}
