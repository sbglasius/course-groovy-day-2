package demo.groovy.day2.ast

class Type1 {
	def doSomething() {"Type1 doSomething"}
}

class Type2 {
	def doSomething() {"Type2 doSomething"}
	def doSomethingElse() {"Type2 doSomethingElse"}
}

class TryDelegate {
    @Delegate Type1 type1 = new Type1();
    @Delegate Type2 type2 = new Type2()
}

def t = new TryDelegate()

println t.doSomethingElse() // --> "Type2 doSomethingElse"
println t.doSomething() // --> "??"

