package demo.groovy.day2.ast

import groovy.transform.CompileStatic

@CompileStatic
class StaticCompiledTest {
    static String foo(String s) {
        'String'
    }

    static String foo(Integer s) {
        'Integer'
    }

    static String foo(Boolean s) {
        'Boolean'
    }

//    static test() {
//        ['foo', 123, true].each {
//            println foo(it)
//        }
//    }

//    static test() {
//        ['foo', 123, true].each {
//            if (it instanceof String) {
//                foo((String) it)
//            } else if (it instanceof Boolean) {
//                foo((Boolean) it)
//            } else if (it instanceof Integer) {
//                foo((Integer) it)
//            }
//        }
//    }

    static test() {
        ['foo', 123, true].each {
            if (it instanceof String) {
                foo(it)
            } else if (it instanceof Boolean) {
                foo(it)
            } else if (it instanceof Integer) {
                foo(it)
            }
        }
    }

}

StaticCompiledTest.test()

