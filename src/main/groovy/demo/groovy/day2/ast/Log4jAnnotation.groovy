package demo.groovy.day2.ast

import groovy.util.logging.Log4j

@Log4j
class LogDemo {
	def execute() {
		log.fatal("Bad output")
	}
}

new LogDemo().execute()
