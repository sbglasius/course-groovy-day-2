package demo.groovy.day2.ast

// Java singleton pattern
public class StringUtil {
    private static final StringUtil instance =
        new StringUtil();

    // I should not be instantiated outside this class
    private StringUtil() {}

    public static StringUtil getInstance() {
        return instance;
    }

    int count(text) {text.size()}
}

assert 15 == StringUtil.instance.count('Groovy & Grails')

// Groovy Singleton pattern
@Singleton
class Util {
    int count(text) {text.size()}
}

assert 15 == Util.instance.count("Groovy & Grails")

try {
    new Util()
} catch (e) {
    assert e instanceof RuntimeException
}
