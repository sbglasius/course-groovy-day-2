package demo.groovy.day2.ast

def str = "Hej"
String.metaClass.reverse = { ->
    delegate.toUpperCase()
}


//@CompileStatic
class MyClass {
     String getTransformedString(String str) {
         "This string is transformed: ${str.reverse()}"
     }
}


println new MyClass().getTransformedString(str)
println "Test".reverse()
