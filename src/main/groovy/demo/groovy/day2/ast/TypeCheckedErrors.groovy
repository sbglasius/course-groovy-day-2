package demo.groovy.day2.ast
//Example (Typo in variable name)

//@groovy.transform.TypeChecked
//class TypoInFieldName {
//    String name = 'Groovy'
//
//    void method() {
//        println naame
//    }
//}

//Example (Typo in GString)
//@groovy.transform.TypeChecked
//class TypoInGString {
//    String name = 'Groovy'
//
//    void method() {
//        println "My name is $naame"
//    }
//}

//Example (Assignment error)
//@groovy.transform.TypeChecked
//class AssignmentToArrayElement {
//    void method() {
//        Date[] arr = new Date[2]
//        arr[0] = new Date() // ok
//        arr[1] = 'Hello'
//    }
//}

//Example (Return type error)
//@groovy.transform.TypeChecked
//class IncorrectReturnType {
//    int method() {
//        'foo'
//    }
//}

//Example (Type Inferrence Error)
//@groovy.transform.TypeChecked
//class GenericsTypeInference {
//    void test() {
//        List<String> list = ['a', 'b', 'c']
//        println list[0].toUpperCase()
//        List<Number> list2 = ['a', 'b', 'c']
//    }
//}

// Example (Cannot infere 'it' type)
//@groovy.transform.TypeChecked
//List<String> wontCompile() {
//   ['foo','bar','baz'].collect { it.toUpperCase() }
//}
//
