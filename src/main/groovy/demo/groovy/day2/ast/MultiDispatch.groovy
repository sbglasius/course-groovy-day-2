package demo.groovy.day2.ast

import groovy.transform.CompileStatic

@CompileStatic
class StaticDispatch {
    static def foo(Object a) { 'OBJECT' }
    static def foo(String a) { 'STRING' }
    static def foo(Date a) { 'DATE' }

    static def bar(Object o) {
        foo(o)
    }

    static void main(String... args) {
        println foo('string') // prints 'STRING'
        println bar('string') // prints 'OBJECT'
        println foo(new Date()) // prints 'DATE'
        println bar(new Date()) // prints 'OBJECT'
    }
}
