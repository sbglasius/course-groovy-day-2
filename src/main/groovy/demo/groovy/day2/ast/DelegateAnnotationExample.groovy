package demo.groovy.day2.ast

class SimpleEvent {
   @Delegate Date when
   @Delegate List<String> attendees = []
   int maxAttendees = 0
   String description
}

def event = new SimpleEvent(when: new Date() + 7, 
description: 'Groovy course', maxAttendees: 8)

// Delegates to attendees.size()
println event.size() // -->  0
// Delegate to when.after()

println event.after(new Date())  
println event.description // -->  Groovy course
println event.maxAttendees // -->  8

// Delegates to attendees.leftShift()
event << 'mrhaki' << 'student1'  
println event.size() // -->  2
println event[0] // -->  'mrhaki'

// Delegates to attendees.minus()
event -= 'student1'  
println event.size() // -->  1

