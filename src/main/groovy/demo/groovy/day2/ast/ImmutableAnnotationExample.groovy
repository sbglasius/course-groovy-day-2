package demo.groovy.day2.ast

import groovy.transform.Immutable

@Immutable class UserAndRoles {
    String username, email
    Date created = new Date()
    List roles
}

def first = new UserAndRoles(username: 'sbg',
        email: 'sbg@host.com',
        roles: ['admin', 'user'])
assert 'sbg' == first.username
assert 'sbg@host.com' == first.email
assert ['admin', 'user'] == first.roles
assert new Date().after(first.created)
try {
    // Properties are readonly.
    first.username = 'new username'
    assert false
} catch (ReadOnlyPropertyException e) {
    assert e
}
try {
    first.roles << 'new role'
    assert false
} catch (UnsupportedOperationException e) {
    assert true
}


def date = Date.parse('y/M/d','2013/11/25')
// Constructor er oprettet i den rækkefølge
// properties er angivet
def second = new UserAndRoles('user', 'test@host.com',
        date, ['user'])
assert 'user' == second.username
assert 'test@host.com' == second.email
assert ['user'] == second.roles
assert '2013/11/25' == second.created.format('yyyy/MM/dd')
assert date == second.created
// Date, Clonables og arrays er kopieret
assert !date.is(second.created)
// toString() implementation er lavet.
assert 'UserAndRoles(user, test@host.com, Mon Nov 25 00:00:00 CET 2013, [user])' == second.toString()


def third = new UserAndRoles(username: 'user',
        email: 'test@host.com', created: date,
        roles: ['user'])
// equals() metoden er også implementeret
// ud fra de properties som findes på klassen
assert third == second

