package demo.groovy.day2.ast

import groovy.transform.ToString
import groovy.transform.TypeChecked

@ToString
class Place {
    String state
}

@TypeChecked
class Greet {
//  def salute( person ) { println "Hello ${person.name}!" }
  def welcome( Place location ) { println "Welcome to ${location.state}!" }
}

g = new Greet()
//g.salude()                //misspelling
//g.welcome( 123 )
