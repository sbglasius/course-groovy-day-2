package demo.groovy.day2.sql

//@Grab(group = 'com.h2database', module = 'h2', version = '1.3.171')
import groovy.sql.Sql

class JDBCExampleGroovy {
    static void main(String[] args) {
        def sql = Sql.newInstance("jdbc:h2:/tmp/test",
                "sa", "", "org.h2.Driver")
        sql.eachRow("SELECT * FROM public.kommune") { row ->
            println "kommune id: ${row.id}, navn: ${row.navn}, areal: ${row.areal}"
        }
    }
}

