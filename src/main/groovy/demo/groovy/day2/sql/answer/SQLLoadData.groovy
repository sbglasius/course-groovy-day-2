package demo.groovy.day2.sql.answer

//@Grab(group = 'com.h2database', module = 'h2', version = '1.3.171')

import groovy.sql.Sql

def sql = Sql.newInstance("jdbc:h2:/tmp/test", "sa", "", "org.h2.Driver")
sql.execute("DROP TABLE IF EXISTS PUBLIC.KOMMUNE")
sql.execute("CREATE TABLE PUBLIC.KOMMUNE(ID INT AUTO_INCREMENT PRIMARY KEY, NAVN VARCHAR(100), AREAL LONG)")
def url = "http://geo.oiorest.dk/kommuner?regionsnr=1081"
def kommuner = new XmlSlurper().parse(url)
kommuner.kommune.each {
	sql.executeInsert("insert into PUBLIC.KOMMUNE (navn, areal) values(${it.navn.toString()}, ${it.areal.toLong()})")
}
sql.commit()
