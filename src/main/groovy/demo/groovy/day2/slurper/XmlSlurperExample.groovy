package demo.groovy.day2.slurper

def xml = '''
<books xmlns:meta="http://meta/book/info" count="3">
 <book id="1">
   <title lang="en">Groovy in Action</title>
   <meta:isbn>1-932394-84-2</meta:isbn>
 </book>
 <book id="2">
   <title lang="en">Groovy Programming</title>
   <meta:isbn>0123725070</meta:isbn>
 </book>
 <book id="3">
   <title>Groovy &amp; Grails</title>
   <!--Not yet available.-->
 </book>
 <book id="4">
   <title>Griffon Guide</title>
 </book>
</books>
'''

def books = new XmlSlurper().parseText(xml)
        .declareNamespace([meta: 'http://meta/book/info'])
println books.getClass().name // --> GPathResult
println books.book.size()     // --> 4
println books.breadthFirst().size() // --> 11
println books.book[0].title.toString() // --> Groovy in Action
println books.book.find {
    it.@id == '2'
}.title.toString()      // --> Groovy Programming
println books.book.findAll {
    it.title =~ /Groovy/
}.'@id'*.toInteger() // [1, 2, 3]
println books.book.'meta:isbn'*.toString()
    // --> [1-932394-84-2, 0123725070]

println books.notexisting.tag.toString()
