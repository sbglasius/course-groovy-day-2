package demo.groovy.day2.httpbuilder

import groovyx.net.http.HttpBuilder

// See http://www.icndb.com/api/
def http = HttpBuilder.configure { request.uri = 'https://dawa.aws.dk/' }

// perform a GET request, expecting JSON response data
def result = http.get {
  request.uri.path = '/regioner'
}

println result

result.each {
  println "${it.kode}: ${it.navn}"
}

result = http.get {
  request.uri.path = '/regioner/1084'
}

println "${result.navn}:"

result = http.get {
  request.uri.path = '/kommuner'
}

result.findAll { it.region.kode == '1084' }.each {
  println "* $it.navn"
}
