package demo.groovy.day2.spock


interface Subscriber {
    int receive(String message)
    int receive(String message, int count)
}
