package demo.groovy.day2.spock


class Publisher {
    @Delegate
    List<Subscriber> subscribers = []

    def send(String message) {
        subscribers.each {
            try {
                it.receive(message)
            } catch (ignored) {}
        }
    }
}
