package demo.groovy.day2.metaprogramming

// Change the meta class of string to satisfy these asserts:

assert "" == "".toCamelCase()
assert "x" == "X".toCamelCase()
assert "thisIsACaseForTheCamel" == "THIS_IS_A_CASE_FOR_THE_CAMEL".toCamelCase()

