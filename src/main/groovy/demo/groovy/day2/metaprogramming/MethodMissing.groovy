package demo.groovy.day2.metaprogramming

String.metaClass.methodMissing = { name, args ->
    if (name == 'rupper') {
        println "Defining new method for rupper"
        def rupper = {->
            delegate.reverse().toUpperCase()
        }
        String.metaClass.rupper = rupper
        rupper.delegate = delegate
        rupper()
    } else {
        throw new MissingMethodException(name, String, args)
    }
}

println "Test".rupper()
println "Knud".rupper()
println "knup".flower()
