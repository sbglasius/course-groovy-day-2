package demo.groovy.day2.metaprogramming.answer

class SmartStorage {
    def storage = [:]

    void setProperty(String name, Object value) {
        storage[name] = value
    }

    Object getProperty(String name) {
        storage[name]
    }
}

def foo = new SmartStorage()

foo.bar = "Baz"

assert "Baz" == foo.bar
