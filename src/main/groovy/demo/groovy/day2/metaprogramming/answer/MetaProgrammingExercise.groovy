package demo.groovy.day2.metaprogramming.answer

String.metaClass.toCamelCase = {->
    delegate.toLowerCase().replaceAll(/_(\w)?/) { wholeMatch, firstLetter ->
        firstLetter?.toUpperCase() ?: ""
    }
}

assert "" == "".toCamelCase()
assert "x" == "X".toCamelCase()
assert "thisIsACaseForTheCamel" == "THIS_IS_A_CASE_FOR_THE_CAMEL".toCamelCase()

/**
// This was the instructors first take at making toCamelCase() - not very groovy!
String.metaClass.toCamelCase = {->
  if(delegate.size() > 1) {
    def str = delegate.toLowerCase().split('_').collect {
      it[0].toUpperCase() + (it.size() > 1 ? it[1..-1] : '')
    }.join('')
    str[0].toLowerCase() + str[1..-1]
  } else {
    delegate.toLowerCase()
  }
}
**/
