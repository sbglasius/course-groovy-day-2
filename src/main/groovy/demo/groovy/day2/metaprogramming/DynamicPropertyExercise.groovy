package demo.groovy.day2.metaprogramming

class SmartStorage {
        // Implement logic that satisfy the assert below.
}

def foo = new SmartStorage()

foo.bar = "Baz"
foo.test = {-> println "test"}

assert "Baz" == foo.bar
