package demo.groovy.day2.metaprogramming

String.metaClass.repeat = { int num ->
    delegate * num
}
println "123".repeat(2)

String test = "Groovy is super cool!"

test.metaClass.rupper ={ ->
    delegate.reverse().toUpperCase()
}
println test.rupper()

"This string will not have rupper".rupper()

