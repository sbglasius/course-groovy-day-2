package demo.groovy.day2.builders
import groovy.xml.MarkupBuilder

def writer = new StringWriter()
def builder = new MarkupBuilder(writer)
builder.html {
    head {
        title 'Simple document'
    }
    body(id: 'main') {
        h1 'Building HTML the Groovy Way'
        p 'Text'
        p {
            mkp.yield 'Text with '
            br()
            strong 'a bold'
            mkp.yield ' element. '
            mkp.yield 'We can also have '
            a href: 'link.html', 'links'
            mkp.yield ' inside a text block.'
            mkp.yield '<invalid/>'
        }
        a href: 'more.html', 'Read more...'
    }
}
println writer
