package demo.groovy.day2.builders.answer

class MyBuilder {
    Map result = [:]
    String currentQuestion
    static build(Closure c) {
        def builder = new MyBuilder()
        c.delegate = builder
        c.call()
        return builder.result
    }

    def question(String question, Closure answerClosure) {
        currentQuestion = question
        answerClosure.delegate = this
        answerClosure.call()
    }

    def answer(String answer){
        result[currentQuestion] = answer
    }
}


assert MyBuilder.build {
    question("Is it easy to create a builder") {
        answer("Yes it is!")
    }
    question("Is it usefull") {
        answer("It could be")
    }
} == ["Is it easy to create a builder": "Yes it is!", "Is it usefull": "It could be"]
