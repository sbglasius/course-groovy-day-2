package demo.groovy.day2.builders



class MyBuilder {
    def result = [:]

    static build(Closure c) {
        def builder = new MyBuilder()
        c.delegate = builder
        c.call()
        return builder.result
    }

    void question(String question, Closure answer) {
        answer.delegate = this
        result[question] = answer.call()
    }

    String answer(String a) {
        return a
    }
}


assert MyBuilder.build {
    question("Is it easy to create a builder") {
        answer("Yes it is!")
    }
    question("Is it usefull") {
        answer("It could be")
    }
} == ["Is it easy to create a builder": "Yes it is!", "Is it usefull": "It could be"]
