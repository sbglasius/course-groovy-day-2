package demo.groovy.day2.builders
import groovy.xml.StreamingMarkupBuilder
import groovy.xml.XmlUtil

//)
class Book {
    int id
    String title
    String language
    String isbn
}

def booksList = [
        new Book(id: 1, title: 'Groovy in Action',
                language: "en", isbn: '1-932394-84-2'),
        new Book(id: 2, title: 'Groovy Programming',
                language: "en", isbn: '0123725070')
]

def builder = new StreamingMarkupBuilder()
builder.encoding = 'UTF-8'
def books = builder.bind {
    mkp.xmlDeclaration()  // <?xml version="1.0" encoding="UTF-8"?>
    namespaces << [meta: 'http://meta/book/info']
    books(count: booksList.size()) {
        booksList.each { b ->
            book(id: b.id) {
                title(lang: b.language, b.title)
                'meta:isbn'(b.isbn)
            }
        }
    // Generate this XML
    /** Format:
     <?xml version="1.0" encoding="UTF-8"?>
     <books xmlns:meta="http://meta/book/info" count="2">
       <book id="1">
         <title lang="en">Groovy in Action</title>
         <meta:isbn>1-932394-84-2</meta:isbn>
       </book>
       <book id="2">
         <title lang="en">Groovy Programming</title>
         <meta:isbn>0123725070</meta:isbn>
       </book>
     </books>
     **/

    }
}

println XmlUtil.serialize(books)
