package demo.groovy.day2.spock


import spock.lang.Specification

class APublisherSpecification extends Specification {
    def pub = new Publisher()
    def sub1 = Mock(Subscriber)
    def sub2 = Mock(Subscriber)

    def setup() {
        pub << sub1 << sub2
    }

    def "delivers events to all subscribers"() {
        when:
            pub.send("event")

        then:
            1 * sub1.receive("event")
            1 * sub2.receive("event")

    }

    def "delivers events to all subscribers the correct number of times"() {
        when:
            3.times {
                pub.send("event")
            }

        then:
            3 * sub1.receive("event")
            3 * sub2.receive("event")
    }

    def "can cope with misbehaving subscribers"() {

        when:
            pub.send("event1")
            pub.send("event2")

        then:
            2 * sub1.receive(_) >> { throw new Exception() }
            1 * sub2.receive("event1")
            1 * sub2.receive("event2")
    }

    def "can cope with misbehaving subscribers - where the receiving parameter is not interesting"() {

        when:
            pub.send("event1")
            pub.send("event2")

        then:
            1 * sub1.receive('event1') >> { throw new Exception() }
            1 * sub1.receive('event2')
            2 * sub2.receive(_)
    }
}
