package demo.groovy.day2.spock

import demo.groovy.day2.Person
import spock.lang.Specification
import spock.lang.Unroll

class HelloSpock extends Specification {
    @Unroll
    def "length of Spock's and his friends' names"() {
        // TODO: Fix this - it's TDD
        given: "When a person is created"
        def person = new Person(name: name)

        when: "Calling the length of that persons name"
        def size = person.toString().size()

        then: "The person length should be"
        size == length

        where:
        name     | length
        "Spock"  | 5
        "Kirk"   | 7
        "Scotty" | 6
    }
}
